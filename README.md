# vlc-bridge-uk

watch UK live streams in VLC

### using

>$ docker run -d -p 7777:7777 --name vlc-bridge-uk registry.gitlab.com/miibeez/vlc-bridge-uk

>$ vlc http://localhost:7777/bbc/playlist.m3u

#### ITV login

>$ docker run -d -p 7777:7777 -e 'ITV_USER=user@email.com' -e 'ITV_PASS=secret' --name vlc-bridge-uk registry.gitlab.com/miibeez/vlc-bridge-uk

>$ vlc http://localhost:7777/itv/playlist.m3u

### notes

support iPlayer (BBC), Channel 4, Blaze, My 5, UKTVPlay, ITV

TV license required
